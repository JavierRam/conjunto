﻿using System;
using System.Collections.Generic;

namespace Calculo_de_conjuntos
{
    class Program
    {
        static void Main()
        {
            // Creando los conjuntos A y B con HashSet
            HashSet<int> NumA = new HashSet<int>();
            HashSet<int> NumB = new HashSet<int>();

            while (true)
            {
                Console.WriteLine("MENU DE OPCIONES");
                Console.WriteLine("1. Ingresar valores al conjunto A");
                Console.WriteLine("2. Ingresar valores al conjunto B");
                Console.WriteLine("3. Mostrar conjuntos A y B");
                Console.WriteLine("4. Calcular unión");
                Console.WriteLine("5. Calcular intersección");
                Console.WriteLine("6. Calcular diferencia");
                Console.WriteLine("7. Calcular diferencia simétrica");
                Console.WriteLine("8. Salir");
                Console.Write("Ingresa la opción deseada: ");

                int opcion;
                if (int.TryParse(Console.ReadLine(), out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            IngresarValores("A", NumA);
                            break;
                        case 2:
                            IngresarValores("B", NumB);
                            break;
                        case 3:
                            MostrarConjuntos(NumA, NumB);
                            break;
                        case 4:
                            CalcularMostrarOperacion("Unión", NumA, NumB, (a, b) => a.UnionWith(b));
                            break;
                        case 5:
                            CalcularMostrarOperacion("Intersección", NumA, NumB, (a, b) => a.IntersectWith(b));
                            break;
                        case 6:
                            CalcularMostrarOperacion("Diferencia", NumA, NumB, (a, b) => a.ExceptWith(b));
                            break;
                        case 7:
                            CalcularMostrarOperacion("Diferencia Simétrica", NumA, NumB, (a, b) => a.SymmetricExceptWith(b));
                            break;
                        case 8:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Opción no válida. Por favor, ingresa un número del 1 al 8.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Opción no válida. Por favor, ingresa un número del 1 al 8.");
                }

                Console.WriteLine();
            }
        }

        static void IngresarValores(string nombreConjunto, HashSet<int> conjunto)
        {
            Console.WriteLine($"Ingresa valores para el conjunto {nombreConjunto}");
            int contador = 0;
            for (int i = 0; i < 7; i++)
            {
                Console.Write($"[{++contador}]: ");
                int valor = int.Parse(Console.ReadLine());
                conjunto.Add(valor);
            }
        }

        static void MostrarConjuntos(HashSet<int> conjuntoA, HashSet<int> conjuntoB)
        {
            MostrarConjunto("A", conjuntoA);
            MostrarConjunto("B", conjuntoB);
        }

        static void MostrarConjunto(string nombreConjunto, HashSet<int> conjunto)
        {
            Console.Write($"Conjunto {nombreConjunto} = {{");
            foreach (int elemento in conjunto)
            {
                Console.Write($"{elemento} ");
            }
            Console.WriteLine("}");
        }

        static void CalcularMostrarOperacion(string nombreOperacion, HashSet<int> conjuntoA, HashSet<int> conjuntoB, Action<HashSet<int>, HashSet<int>> operacion)
        {
            HashSet<int> resultado = new HashSet<int>(conjuntoA);
            operacion(resultado, conjuntoB);

            MostrarConjuntos(conjuntoA, conjuntoB);
            MostrarConjunto(nombreOperacion, resultado);
        }
    }
}
